package com.ksudev.gitmvp.repository;

import android.support.annotation.NonNull;

import java.util.List;

import com.ksudev.gitmvp.content.Authorization;
import com.ksudev.gitmvp.content.Repository;
import rx.Observable;

/**
 * @author nevazno
 */
public interface GithubRepository {

    @NonNull
    Observable<List<Repository>> repositories();

    @NonNull
    Observable<Authorization> auth(@NonNull String login, @NonNull String password);
}
