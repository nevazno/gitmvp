package com.ksudev.gitmvp.repository;

import android.support.annotation.NonNull;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import ru.arturvasilov.rxloader.RxUtils;
import com.ksudev.gitmvp.api.ApiFactory;
import com.ksudev.gitmvp.content.Authorization;
import com.ksudev.gitmvp.content.Repository;
import com.ksudev.gitmvp.utils.AuthorizationUtils;
import com.ksudev.gitmvp.utils.PreferenceUtils;
import rx.Observable;
import rx.functions.Func1;

/**
 * @author nevazno
 */
public class DefaultGithubRepository implements GithubRepository {

    @NonNull
    @Override
    public Observable<List<Repository>> repositories() {
        return ApiFactory.getGithubService()
                .repositories()
                .flatMap(new Func1<List<Repository>, Observable<? extends List<Repository>>>() {
                    @Override
                    public Observable<? extends List<Repository>> call(List<Repository> repositories) {
                        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.delete(Repository.class);
                                realm.insert(repositories);
                            }
                        });
                        return Observable.just(repositories);
                    }
                })
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends List<Repository>>>() {
                    @Override
                    public Observable<? extends List<Repository>> call(Throwable throwable) {
                        Realm realm = Realm.getDefaultInstance();
                        RealmResults<Repository> repositories = realm.where(Repository.class).findAll();
                        return Observable.just(realm.copyFromRealm(repositories));
                    }
                })
                .compose(RxUtils.async());
    }

    @NonNull
    public Observable<Authorization> auth(@NonNull String login, @NonNull String password) {
        String authorizationString = AuthorizationUtils.createAuthorizationString(login, password);
        return ApiFactory.getGithubService()
                .authorize(authorizationString, AuthorizationUtils.createAuthorizationParam())
                .flatMap(new Func1<Authorization, Observable<? extends Authorization>>() {
                    @Override
                    public Observable<? extends Authorization> call(Authorization authorization) {
                        PreferenceUtils.saveToken(authorization.getToken());
                        PreferenceUtils.saveUserName(login);
                        ApiFactory.recreate();
                        return Observable.just(authorization);
                    }
                })
                .compose(RxUtils.async());
    }
}
