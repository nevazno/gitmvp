package com.ksudev.gitmvp.screen.auth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.ksudev.gitmvp.R;

import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.arturvasilov.rxloader.LifecycleHandler;
import ru.arturvasilov.rxloader.LoaderLifecycleHandler;

public class AuthActivity extends AppCompatActivity {

    private AuthViewHolder holder;
    private AuthPresenter mPresenter;

    public static void start(@NonNull Activity activity) {
        Intent intent = new Intent(activity, AuthActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        ButterKnife.bind(this);
        View parentView = getWindow().getDecorView().getRootView();
        holder = new AuthViewHolder(parentView);
        AuthView view = new AuthView(holder, this);
        LoaderManager loaderManager = LoaderManager.getInstance(this);
        LifecycleHandler lifecycleHandler = LoaderLifecycleHandler.create(this, loaderManager);
        mPresenter = new AuthPresenter(lifecycleHandler, view);
        mPresenter.init();
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.logInButton)
    public void onLogInButtonClick() {
        String login = holder.mLoginEdit.getText().toString();
        String password = holder.mPasswordEdit.getText().toString();
        mPresenter.tryLogIn(login, password);
    }
}
