package com.ksudev.gitmvp.screen.auth;

import com.ksudev.gitmvp.screen.general.LoadingView;

/**
 * @author nevazno
 */
public interface IAuthView extends LoadingView {

    void openRepositoriesScreen();

    void showLoginError();

    void showPasswordError();

}