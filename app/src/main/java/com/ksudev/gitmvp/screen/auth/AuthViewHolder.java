package com.ksudev.gitmvp.screen.auth;

import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.ksudev.gitmvp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuthViewHolder {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.loginEdit)
    EditText mLoginEdit;

    @BindView(R.id.passwordEdit)
    EditText mPasswordEdit;

    @BindView(R.id.loginInputLayout)
    TextInputLayout mLoginInputLayout;

    @BindView(R.id.passwordInputLayout)
    TextInputLayout mPasswordInputLayout;

    @BindView(R.id.progressBar)
    ProgressBar mLoadingView;

    public AuthViewHolder(View parentView) {
        ButterKnife.bind(this, parentView);
    }
}
