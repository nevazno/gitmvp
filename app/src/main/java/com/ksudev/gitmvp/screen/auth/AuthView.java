package com.ksudev.gitmvp.screen.auth;

import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.ksudev.gitmvp.R;
import com.ksudev.gitmvp.screen.repositories.RepositoriesActivity;

public class AuthView implements IAuthView {

    private AuthViewHolder holder;
    private AppCompatActivity activity;

    public AuthView(AuthViewHolder holder, AppCompatActivity activity) {
        this.holder = holder;
        this.activity = activity;
        activity.setSupportActionBar(holder.mToolbar);
        initFields();
    }

    private void initFields(){

    }

    @Override
    public void showLoading() {
        holder.mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        holder.mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoginError() {
        holder.mLoginInputLayout.setError(activity.getString(R.string.error));
    }

    @Override
    public void showPasswordError() {
        holder.mPasswordInputLayout.setError(activity.getString(R.string.error));
    }

    @Override
    public void openRepositoriesScreen() {
        RepositoriesActivity.start(activity);
        activity.finish();
    }
}
