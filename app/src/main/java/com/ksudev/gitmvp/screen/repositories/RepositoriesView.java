package com.ksudev.gitmvp.screen.repositories;

import android.support.annotation.NonNull;

import java.util.List;

import com.ksudev.gitmvp.content.Repository;
import com.ksudev.gitmvp.screen.general.LoadingView;

/**
 * @author nevazno
 */
public interface RepositoriesView extends LoadingView {

    void showRepositories(@NonNull List<Repository> repositories);

    void showCommits(@NonNull Repository repository);

    void showError();
}
