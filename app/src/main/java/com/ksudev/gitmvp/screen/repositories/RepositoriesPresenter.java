package com.ksudev.gitmvp.screen.repositories;

import android.support.annotation.NonNull;
import android.util.Log;

import ru.arturvasilov.rxloader.LifecycleHandler;
import rx.functions.Action0;
import rx.functions.Action1;

import com.ksudev.gitmvp.R;
import com.ksudev.gitmvp.content.Repository;
import com.ksudev.gitmvp.repository.RepositoryProvider;

import java.util.List;

/**
 * @author nevazno
 */
public class RepositoriesPresenter {

    private final LifecycleHandler mLifecycleHandler;
    private final RepositoriesView mView;

    public RepositoriesPresenter(@NonNull LifecycleHandler lifecycleHandler,
                                 @NonNull RepositoriesView view) {
        mLifecycleHandler = lifecycleHandler;
        mView = view;
    }

    public void init() {
        RepositoryProvider.provideGithubRepository()
                .repositories()
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        Log.e("RepositoriesPresenter", "showLoading");
                        mView.showLoading();
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        Log.e("RepositoriesPresenter", "hideLoading");
                        mView.hideLoading();
                    }
                })
                .compose(mLifecycleHandler.load(R.id.repositories_request))
                .subscribe(new Action1<List<Repository>>() {
                    @Override
                    public void call(List<Repository> repositories) {
                        mView.showRepositories(repositories);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        mView.showError();
                    }
                });
    }

    public void onItemClick(@NonNull Repository repository) {
        mView.showCommits(repository);
    }
}
