package com.ksudev.gitmvp.screen.general;

/**
 * @author nevazno
 */
public interface LoadingView {

    void showLoading();

    void hideLoading();

}