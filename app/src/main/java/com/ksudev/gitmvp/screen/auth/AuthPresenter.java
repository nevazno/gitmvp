package com.ksudev.gitmvp.screen.auth;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import ru.arturvasilov.rxloader.LifecycleHandler;
import rx.functions.Action0;
import rx.functions.Action1;

import com.ksudev.gitmvp.R;
import com.ksudev.gitmvp.content.Authorization;
import com.ksudev.gitmvp.repository.RepositoryProvider;
import com.ksudev.gitmvp.utils.PreferenceUtils;

/**
 * @author nevazno
 */
public class AuthPresenter {

    private final LifecycleHandler mLifecycleHandler;
    private final IAuthView mAuthView;

    public AuthPresenter(@NonNull LifecycleHandler lifecycleHandler,
                         @NonNull IAuthView authView) {
        mLifecycleHandler = lifecycleHandler;
        mAuthView = authView;
    }

    public void init() {
        String token = PreferenceUtils.getToken();
        if (!TextUtils.isEmpty(token)) {
            mAuthView.openRepositoriesScreen();
        }
    }

    public void tryLogIn(@NonNull String login, @NonNull String password) {
        if (TextUtils.isEmpty(login)) {
            mAuthView.showLoginError();
        } else if (TextUtils.isEmpty(password)) {
            mAuthView.showPasswordError();
        } else {
            RepositoryProvider.provideGithubRepository()
                    .auth(login, password)
                    .doOnSubscribe(new Action0() {
                        @Override
                        public void call() {
                            Log.e("RepositoriesPresenter", "showLoading");
                            mAuthView.showLoading();
                        }
                    })
                    .doOnTerminate(new Action0() {
                        @Override
                        public void call() {
                            Log.e("RepositoriesPresenter", "hideLoading");
                            mAuthView.hideLoading();
                        }
                    })
                    .compose(mLifecycleHandler.reload(R.id.auth_request))
                    .subscribe(new Action1<Authorization>() {
                                   @Override
                                   public void call(Authorization authorization) {
                                       mAuthView.openRepositoriesScreen();
                                   }
                               },
                            new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {
                                    mAuthView.showLoginError();
                                }
                            });
        }
    }
}
